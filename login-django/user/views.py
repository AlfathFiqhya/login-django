from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from .forms import CustomUserCreationForm
from django.contrib import auth
# Register
def register(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, 'Account created successfully.. Login now!')
        else:
            messages.warning(request, "Password don't match")
        return redirect('register')
    else:
        form = CustomUserCreationForm()
    return render(request, 'user/register.html', {'form': form})
# Login
def login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = auth.authenticate(username=username, password=password)

        if user is not None:
            # correct username and password login the user
            auth.login(request, user)
            return redirect('user_page')

        else:
            messages.warning(request, 'Error wrong username/password')
            
    return render(request, 'user/login.html')
    
def logout(request):
    auth.logout(request)
    return redirect('login')
    return render(request,'user/login.html')


def user_page(request):
    return render(request, 'user/user.html')
